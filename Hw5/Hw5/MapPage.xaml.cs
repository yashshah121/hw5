﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hw5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        public MapPage()
        {
            InitializeComponent();
        }
    }
}*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
//This file is for SatelliteView
//reference.
/* https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/picker/populating-items. 
 https://docs.microsoft.com/en-us/dotnet/api/xamarin.forms.picker?view=xamarin-forms
 https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/picker/populating-itemssource
 https://stackoverflow.com/questions/55108443/xamarin-forms-navigationpage-titleview-need-to-be-on-all-pages
 https://stackoverflow.com/questions/55058600/how-to-call-a-xaml-code-in-navigationpage-settitleview-in-xamarin-forms
     
     
     
     
     
     
     */
namespace Hw5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        ObservableCollection<Pin> pinNames;

        public MapPage()
        {
            InitializeComponent();
            MainMap.MapType = MapType.Satellite;
           
            ////Sets the initial map load location with the radies of 10 miles.
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                                (new Position(33.1307785, -117.1601826)
                                                 , Distance.FromMiles(10));

            MainMap.MoveToRegion(initialMapLocation);
            //calling picker function
            PopulatePicker();
            
        }
        private void PopulatePicker()
        {
            //Adding 5 pins in the list
            pinNames = new ObservableCollection<Pin>
            {
               new Pin
                {

                Label = "North Bar Monday Night",
                Address = "200 W El Norte Pkwy #7, Escondido, CA 92026",
                Type = PinType.SavedPin,
                Position = new Position(33.139831, -117.0938797)

                },
                new Pin
                {
                Position = new Position(33.1299158, -117.1674921),
                Label = "Players sports Bar. Tuesday Night",
                Address = "328 S Twin Oaks Valley Rd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.1354103, -117.1868887),
                Label = "Churchils Wednesday Night",
                Address = "887 W San Marcos Blvd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.1861666, -117.3294044),
                Label = "Rookies",
                Address = "887 W San Marcos Blvd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.0453442, -117.2962403),
                Label = "Saloon",
                Address = "887 W San Marcos Blvd, San Marcos, CA 92078",
                Type = PinType.SavedPin
                }
            };
            //Adding it to picker
            picker.Items.Add("Select which night you want to go out");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                picker.Items.Add(item.Label);
                MainMap.Pins.Add(item);
            }
            //setting index to 0.
            picker.SelectedIndex = 0;
            picker.SelectedIndexChanged += (sender, e) =>
            {
                var picker = (Picker)sender;
                string pname = picker.SelectedItem.ToString();
                
                foreach (Pin item in pinNames)
                {
                    if (item.Label == pname)
                    {
                        //Move the map to center on the pin
                        MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                    }
                }
            };

        }
       
       
       
    }
}
