﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
//This file is for StreetView.
namespace Hw5
{

    public partial class Street : ContentPage
    {
        ObservableCollection<Pin> pinNames;
        public Street()
        {
            InitializeComponent();
            //Creating the reference
            customMap.MapType = MapType.Street;
            //Sets the initial map load location with the radies of 11 miles.
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                                (new Position(33.1002751, -117.2063601)
                                                 , Distance.FromMiles(11));

            customMap.MoveToRegion(initialMapLocation);
            //calling picker function
            PopulatePicker();
        }
        private void PopulatePicker()
        {
            //Adding 5 pins in the list
            pinNames = new ObservableCollection<Pin>
            {
                new Pin
                {
                
                Label = "Vallarta Express",
                Address = "9313 Mira Mesa Blvd, San Diego, CA 92126, United States",
                Type = PinType.SavedPin,
                Position = new Position(33.156558,-117.3268174)
                    
                },
                new Pin
                {
                Position = new Position(33.1374357, -117.3253408),
                Label = "Rim Talay Thai Cuisine",
                Address = "508 Mission Ave, Oceanside, CA 92054, United States",
                Type = PinType.SavedPin  
                },
                new Pin
                {
                Position = new Position(33.151033,-117.1938047),
                Label = "Sorrento's Pizza",
                Address = "1450 W Mission Rd D, San Marcos, CA 92069, United States",
                Type = PinType.SavedPin   
                },
                new Pin
                {
                Position = new Position(33.1439315,-117.1917174),
                Label = "Curry & More - Indian Bistro",
                Address = "113 S Las Posas Rd #111, San Marcos, CA 92078, United States",
                Type = PinType.SavedPin
                },
                new Pin
                {
                Position = new Position(33.1614266, -117.3490849),
                Label = "Harumama Noodles & Buns",
                Address = "2958 Madison St, Carlsbad, CA 92008, United States",
                Type = PinType.SavedPin
                }
            };
            //Adding it to picker
            picker.Items.Add("Select which night you want to go out");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                picker.Items.Add(item.Label);
                customMap.Pins.Add(item);
            }
            //setting index to 0.
            picker.SelectedIndex = 0;
            picker.SelectedIndexChanged += (sender, e) =>
            {
                var picker = (Picker)sender;
                string pname = picker.SelectedItem.ToString();
                
                foreach (Pin item in pinNames)
                {
                    if (item.Label == pname)
                    {
                        //Move the map to center on the pin
                        customMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                    }
                }
            };

        }
        

        
        
    }
}